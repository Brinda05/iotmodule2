#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<shunyaInterfaces.h>
#include<dirent.h>

//For Meter connections using RS485

Rishimeter=opendir("/dev/ttyAMA0");

int main(void)
{
    //initialization of shunya interface
    shunyaInterfacesetup();
    //modbus IP configurtion
    "modbus-tcp":{"type":"tcp","ipaddr":" ","port":" "};

    //modbus connect read or write data
    modbusObj Rishimeter = newModbus("modbus-tcp");//for creating new instance
    modbusTcpConnect(&Rishimeter);

    // reading data of different parameters
    float ac_current = modbusTcpRead(&Rishimeter,30007);
    float power = modbusTcpRead(&Rishimeter, 30013);
    float Active_energy = modbusTcpRead(&Rishimeter, 30147);
    float Reactive_energy = modbusTcpRead(&Rishimeter, 30151);
    float Apparent_energy = modbusTcpRead(&Rishimeter, 30081);
    float voltage = modbusTcpRead(&Rishimeter, 30001);
    float THD_of_Voltage = modbusTcpRead(&Rishimeter, 30219);
    
    return 0;
}




